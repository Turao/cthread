#include <stdlib.h>
#include <stdio.h>

#include "../include/cthread.h"

#define SUCCESS 0
#define ERROR -1

void* sum();
void* sub();
void* print();

csem_t sem;
int a;

void* sum()
{
  printf("\n > entering sum \n ");
  int i;
  if(cwait(&sem) == SUCCESS) {
    for(i = 0; i < 20; i++) {
      a += 1;
      printf("a: %d \t... ", a);
      printf("yielding... ");
      cyield();
      printf("returning to sum from yield: +1\n");
    }
    printf("\n");
    csignal(&sem);
  }  

  return NULL;
}


void* sub()
{
  printf("\n > entering sub \n");
  int i;
  if(cwait(&sem) == SUCCESS) {
    for(i = 0; i < 20; i++) {
      a -= 1;
      printf("a: %d \t... ", a);
      printf("yielding... ");
      cyield();
      printf("returning to sub from yield: -1\n");
    }
    printf("\n");
    csignal(&sem);
  }

  return NULL;
}


int main()
{
  a = 0;

  if(csem_init(&sem, 1) == SUCCESS) // inicia um semaforo com 1 recurso
  {

    int sum_tid = ccreate(&sum, NULL, 1);
    int sub_tid = ccreate(&sub, NULL, 1);

    cjoin(sub_tid);
    cjoin(sum_tid);
  }
  else printf("erro ao inicializar semaforo!\n");

  return 0;
}