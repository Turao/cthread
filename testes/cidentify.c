#include <stdlib.h>
#include <stdio.h>

#include "../include/cthread.h"

#define MAX_CHAR_CIDENTIFY 256

int main()
{
  char devs[256];
  if(cidentify(devs, sizeof(devs)) == 0) printf("devs:\n%s\n", devs);
  else printf("Erro ao identificar desenvolvedores\n");
  return 0;
}