#include <stdlib.h>
#include <stdio.h>

#include "../include/cthread.h"

void* print();
void* fancy_print();


void* print()
{
  printf("print!\n");
  return NULL;
}


void* fancy_print()
{
  printf("fancy print!\n");
  return NULL;
}


int main()
{
  int print_tid = ccreate(&print, NULL, 0);
  int fancy_print_tid = ccreate(&fancy_print, NULL, 1);

  csetprio(fancy_print_tid, 0);
  csetprio(print_tid, 1);

  cjoin(print_tid);

  return 0;
}