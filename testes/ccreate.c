#include <stdlib.h>
#include <stdio.h>

#include "../include/cthread.h"

void* myFunc()
{
  printf("myFunc created!\n");
  return NULL;
}

int main()
{
  printf("\n");
  int i;
  for(i = 0; i<100; i++)
  {
    int tid = ccreate(&myFunc, NULL, i%5);
    if(tid > 0)  printf("Teste %d - Thread criada com TID: %d e prioridade %d\n", i, tid, i%5);
    else printf("Nao foi possivel criar thread (prio %d)\n", i%5);
  }

  printf("\n");

  return 0;
}