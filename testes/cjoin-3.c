#include <stdlib.h>
#include <stdio.h>

#include "../include/cthread.h"

void* fn();
void* fn2();
void* fn3();

void* fn()
{
  printf("fn created\n");

  int fn2_tid = ccreate(&fn2, NULL, 1);
  ccreate(&fn3, NULL, 1);
  printf("fn starts waiting for fn2 (tid %d)...\n", fn2_tid);
  cjoin(fn2_tid);

  printf("terminating fn\n");
  return NULL;
}

void* fn2()
{
  printf("fn2 created\n");
  cyield(); 
  // libera a cpu
  // agora, threads "main" e "fn" estao esperando outras threads
  // o fluxo deve ser jogado para fn3 ou fn2
  printf("terminating fn2\n");
  return NULL;
}

void* fn3()
{
  printf("fn3 created\n");
  cyield();
  // libera a cpu
  // agora, threads "main" e "fn" estao esperando outras threads
  // o fluxo deve ser jogado para fn3 ou fn2
  printf("terminating fn3\n");
  return NULL;
}

int main()
{
  printf("main begins...\n");
  int fn_tid = ccreate(&fn, NULL, 1);
  printf("main starts waiting for fn (tid %d)...\n", fn_tid);
  cjoin(fn_tid);

  printf("terminating main\n");

  return 0;
}