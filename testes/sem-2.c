#include <stdlib.h>
#include <stdio.h>

#include "../include/cthread.h"

#define SUCCESS 0
#define ERROR -1

csem_t *sem;
csem_t *sem1;
csem_t *sem2;
csem_t sem3;

int main()
{
  if(csem_init(sem, 1) == SUCCESS) printf("[sem] initialized!\n");
  else printf("[sem] error on initialization\n");
  free(sem);


  if(csem_init(sem1, 0) == SUCCESS) printf("[sem1] initialized!\n");
  else printf("[sem1] error on initialization\n");
  free(sem1);
  

  if(csem_init(sem2, -1) == SUCCESS) printf("[sem2] initialized!\n");
  else printf("[sem2] error on initialization\n");
  free(sem2);
  

  if(csem_init(&sem3, 1) == SUCCESS) printf("[sem3] initialized!\n");
  else printf("[sem3] error on initialization\n");


  return 0;
}