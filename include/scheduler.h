#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "list.h"
#include "utils.h"

#include "cdata.h"
#include "cthread.h"
#include "waiting.h"

#define SUCCESS 0 // required cavemen stuff
#define ERROR -1 // required cavemen stuff

#define _RAISE_NEXT_THREAD_ERROR() {STDERR_INFO(" * Unable to get next thread \n"); return ERROR;}
#define _RAISE_RUNNING_THREAD_ERROR() {STDERR_INFO(" * Unable to get running thread \n"); return ERROR;}
#define _RAISE_DOESNT_EXIST_ERROR(tid) {STDERR_INFO(" * Thread %d doesn't exist \n", tid); return ERROR;}

/* Enum de estados possiveis para
*  uma thread
*/
enum {
  CRIACAO = 0,
  APTO = 1,
  EXECUCAO = 2,
  BLOQUEADO = 3,
  TERMINO = 4
};


/* Enum de estados possiveis para
*  um semaforo
*/
enum {
  LOCKED = 0,
  UNLOCKED = 1
};


/* Estrutura de lista de aptos
*  contendo quatro niveis de prioridade
*/
typedef struct Apts {
  List p0; // highest priority
  List p1;
  List p2;
  List p3; // lowest priority
} Apts;




/***********************************************/
/********* SCHEDULER MAIN FUNCTIONS ************/

/* DISPATCHER : runThread 
*  o nome so muda para melhorar a semantica
*  a legibilidade do codigo
*
*  Responsavel pela troca de contexto
*  entre thread em execucao e thread
*  escolhida pelo escalonador
*/
void runThread(TCB_t* current_thread, TCB_t* next_thread);

/* SCHEDULER : getNextThread 
*  o nome so muda para melhorar a semantica
*  a legibilidade do codigo
*
*  Responsavel pela escolha da proxima thread
*  atraves de um sistema de filas com multiplas
*  prioridades e politica FILO
*/
TCB_t* getNextThread();

/* Define a thread em execucao e altera 
*  o estado da tcb para EXECUTANDO
*/
void setRunningThread(TCB_t* thread);

/* Responsavel pelo tratamento de uma thread
*  apos o termino de sua execucao
*  libera a memoria e chama o escalonador
*/
int32_t terminateThread();

/* Pesquisa nas estruturas de dados alocadas
*  se uma thread existe
*/
TCB_t* exists(int32_t tid);
bool _compare_tid(void* thread, void* tid);


/* Limpa a memoria, desalocando as estruturas 
*  alocadas durante a execucao da biblioteca
*/
void _cleanup();



/***********************************************/
/******** Apts List related functions***********/

/* Retorna a lista de aptos ativos */
Apts* getActive();

/* Insere uma thread na fila de aptos ativos */
void enqueueApts(TCB_t* thread);

/* Insere uma thread na devida fila de aptos 
*  (de acordo com sua prioridade) 
*/
void _enqueue(Apts* queue, TCB_t* thread);

/* Re-insere uma thread na devida fila de aptos
*  de acordo com a sua nova prioridade
*/
int32_t updatePriorityQueue(TCB_t* thread);


/***********************************************/
/************* 'Attr' getters ******************/

/* Retorna um TID valido 
*  (nao utilizado por outra thread) 
*/
int32_t get_new_TID();

/* Retorna a thread em execucao */
TCB_t* getRunningThread();




/***********************************************/
/************* Print functions *****************/

/* Imprime o conteudo de uma thread */
bool print_thread(void* thread);




/***********************************************/
/***************** YIELD ***********************/

/* Desativa a thread em execucao,
*  passando para a lista de aptos ativos 
*  para ser escalonada posteriormente
*/
int32_t deactivateRunningThread();




/***********************************************/
/**************** SEMAFORO *********************/

int32_t lockThreadForMutex(csem_t *sem, TCB_t* thread);
int32_t unlockMutexThread(csem_t *sem);




/***********************************************/
/****************** WAIT ***********************/

/* Bloqueia a thread em execucao, passando-a para a lista de espera */
int32_t lockRunningThread();

/* Bloqueia a thread de TID tid, passando-a para a lista de aptos ativos */
int32_t unlockThread(int32_t tid);

/* Retorna a lista threads em espera */
List* getWaiting();

/* Retorna a lista threads em bloqueadas */
List* getLocked();

#endif // __SCHEDULER__