#ifndef __UTILS_H
#define __UTILS_H

#include <stdio.h>

#ifdef DEBUG
#define DEBUG_PRINT(format, ...) \
    fflush(stdout); \
    fprintf(stderr, "[DEBUG]: "); \
    fprintf(stderr, format, ##__VA_ARGS__); \
    fprintf(stderr, "\t > File: %s \n", __FILE__); \
    fprintf(stderr, "\t > Function: %s \n", __func__); \
    fprintf(stderr, "\t > Line: %d \n", __LINE__)
#else
#define DEBUG_PRINT(format, ...) ;
#endif

#ifdef ERRORS_INFO
#define STDERR_INFO(format, ...) \
    fflush(stdout); \
    fprintf(stderr, "[ERROR]: "); \
    fprintf(stderr, format, ##__VA_ARGS__); \
    fprintf(stderr, "\t > File: %s \n", __FILE__); \
    fprintf(stderr, "\t > Function: %s \n", __func__); \
    fprintf(stderr, "\t > Line: %d \n", __LINE__)
#else
#define STDERR_INFO(format, ...)
#endif

#endif