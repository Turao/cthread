#ifndef __WAITINGLIST__
#define __WAITINGLIST__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "list.h"
#include "cdata.h"

/* Informacao de espera:
*  contem a informacao das threads em espera
*  possuindo a thread que esta esperando (waiting)
*  e a thread que esta sendo esperada (beingWaited)
*/
typedef struct WaitingInfo
{
  int32_t waiting;
  int32_t beingWaited;
} WaitingInfo;


/* Ve se uma tcb esta sendo esperada por outra
*  retorna a 'informacao de espera' caso seja,
*  retorna NULL caso nao exista a tcb na lista de espera
*/
WaitingInfo* waitingList_isBeingWaited(List* waitingList, int32_t tid);

/* Comparador usado como argumento na funcao find, da list.c
*  encontra uma thread de id 'tid' que esta sendo esperada
*  por outra
*/
bool _being_waited(void* waitingInfo, void* tid);


/* Ve se uma tcb esta esperando outra
*  retorna a 'informacao de espera' caso seja,
*  retorna NULL caso nao exista a tcb na lista de espera
*/
WaitingInfo* waitingList_isWaiting(List* waitingList, int32_t tid);

/* Comparador usado como argumento na funcao find, da list.c
*  encontra uma thread de id 'tid' que esta esperando outra
*/
bool _is_waiting(void* waitingInfo, void* tid);

#endif // __WAITINGLIST__