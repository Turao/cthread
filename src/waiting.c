#include "waiting.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

#include "list.h"
#include "cdata.h"

/* Ve se uma tcb esta sendo esperada por outra
*  retorna a 'informacao de espera' caso seja,
*  retorna NULL caso nao exista a tcb na lista de espera
*/
WaitingInfo* waitingList_isBeingWaited(List* waitingList, int32_t tid)
{
  if(waitingList == NULL) return NULL;
  if(list_empty(waitingList)) return NULL;

  WaitingInfo** info_ptr = (WaitingInfo**) list_find(waitingList, _being_waited, &tid);
  return info_ptr != NULL ? *info_ptr : NULL;
}


/* Ve se uma tcb esta esperando outra
*  retorna a 'informacao de espera' caso seja,
*  retorna NULL caso nao exista a tcb na lista de espera
*/
WaitingInfo* waitingList_isWaiting(List* waitingList, int32_t tid)
{
  if(waitingList == NULL) return NULL;
  if(list_empty(waitingList)) return NULL;

  WaitingInfo** info_ptr = (WaitingInfo**) list_find(waitingList, _is_waiting, &tid);
  return info_ptr != NULL ? *info_ptr : NULL;
}



bool _being_waited(void* waitingInfo, void* tid)
{
  WaitingInfo* info = *(WaitingInfo**) waitingInfo;
  return info->beingWaited == *(int32_t*) tid ? true : false;
}

bool _is_waiting(void* waitingInfo, void* tid)
{
  WaitingInfo* info = *(WaitingInfo**) waitingInfo;
  return info->waiting == *(int32_t*) tid ? true : false;
}