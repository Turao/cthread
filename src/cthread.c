#include <stdio.h>
#include <stdlib.h>

#include <stdint.h> // howto-c (as of 2016)
#include <stdbool.h> // because we are not cavemen anymore

#include <string.h>

#include <ucontext.h>
#include "list.h"

// #define ERRORS_INFO 1
#include "utils.h"

#include "cthread.h"
#include "cdata.h"

#include "waiting.h"
#include "scheduler.h"


#define DEVELOPERS \
"Arthur Lenz 218316 \n \
Rafael Allegretti 215020 \n \
Guillermo Falcao 217434"
char devs[] = DEVELOPERS;


// TO-DO: REPLACE RETURN TYPE OF FUNCTIONS
// FROM: INT/INT32_T
// TO: BOOL

bool _mainTCB_created = false;

// we bind a context (_exit_context) to a scheduler function (terminateThread)
// this way, everytime a running thread ends its execution
// we switch to the _exit_context, which will call it's binded function
// signaling the scheduler that the current thread ended
ucontext_t* _exit_context;




int32_t _allocExitContext()
{
  _exit_context = (ucontext_t*) calloc(1, sizeof(ucontext_t));
  if(_exit_context == NULL) return ERROR; // calloc error

  _exit_context->uc_link = NULL;
  _exit_context->uc_stack.ss_sp = (uint8_t*) calloc(1, sizeof(SIGSTKSZ));
  if(_exit_context->uc_stack.ss_sp == NULL) {
    STDERR_INFO("Exit context's stack allocation failure \n");
    free(_exit_context); // avoiding memory leak
    return ERROR;
  }
  _exit_context->uc_stack.ss_size = SIGSTKSZ;
  

  // filling other attributes (that are not used by the scheduler)
  getcontext(_exit_context);

  // scheduler function bind
  makecontext(_exit_context, (void (*)(void)) terminateThread, 0, NULL);
  return SUCCESS;
}




/* Creates a control block for the main thread 
*  with the highest priority
*/
int32_t _createMainTCB()
{
  if(_exit_context == NULL) {
    if(_allocExitContext() == ERROR) return ERROR;
  }

  TCB_t* mainThread = (TCB_t*) calloc(1, sizeof(TCB_t));
  if (mainThread == NULL) {
    STDERR_INFO("Main thread control block allocation failure \n");
    return ERROR;
  }


  DEBUG_PRINT(" * Creating main thread control block \n");
  mainThread->state = EXECUCAO;
  mainThread->prio = 0; //to-do: check if should be zero
  mainThread->tid = 0;

  mainThread->context.uc_link = _exit_context;
  mainThread->context.uc_stack.ss_sp = (uint8_t*) calloc(1, sizeof(SIGSTKSZ));
  if(mainThread->context.uc_stack.ss_sp == NULL)
  {
    STDERR_INFO("Main thread context memory allocation failure \n");
    free(mainThread); // avoiding memory leak
    return ERROR;
  }
  mainThread->context.uc_stack.ss_size = SIGSTKSZ;

  getcontext(&mainThread->context);
  _mainTCB_created = true;
  setRunningThread(mainThread);    

  return SUCCESS;
}




int ccreate (void* (*start)(void*), void *arg, int prio)
{
  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  if(_exit_context == NULL) {
    if(_allocExitContext() == ERROR) return ERROR;
  }

  DEBUG_PRINT(" * Creating thread...   \n");

  TCB_t* newThread = (TCB_t*) calloc(1, sizeof(TCB_t));
  if(newThread == NULL) return ERROR; // calloc error
  
  // validate priority
  if(0 <= prio && prio <= 3) {
    newThread->prio = prio;
  } 
  else {
    STDERR_INFO("Invalid priority \n");
    free(newThread); // avoiding memory leak
    return ERROR;
  }

  newThread->tid = get_new_TID();
  newThread->state = APTO;
  // set the new thread exit context as the global exit context
  // when this thread ends, it will switch to the _exit_context 
  // and the terminate thread function linked with _exit_context will be called
  // signaling the scheduler that this thread has ended
  newThread->context.uc_link = _exit_context;


  newThread->context.uc_stack.ss_sp = (uint8_t*) calloc(1, sizeof(SIGSTKSZ));
  if(newThread->context.uc_stack.ss_sp == NULL) {
    STDERR_INFO("New thread's stack allocation failure \n");
    free(newThread);
    return ERROR;
  }
  newThread->context.uc_stack.ss_size = SIGSTKSZ;
  
  // fill other things related to the current context 
  // (not used by the scheduler)
  getcontext(&newThread->context);

  // binds the thread's function with it's respective context
  makecontext(&newThread->context, (void (*)(void)) start, 1, (void*) arg);

  enqueueApts(newThread);
  return newThread->tid;
}




int csetprio(int tid, int prio)
{
  if(prio < 0 || 3 < prio) return ERROR;

  TCB_t* thread = exists(tid);
  if(thread == NULL) _RAISE_DOESNT_EXIST_ERROR(tid);

  if(thread != getRunningThread()) {
    thread->prio = prio;
    // if thread is not running, and is apt
    // we must change its priority queue
    updatePriorityQueue(thread);
  }

  return SUCCESS;
}




int cyield(void)
{
  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  TCB_t* runningThread = getRunningThread();
  if(runningThread == NULL) _RAISE_RUNNING_THREAD_ERROR();

  runningThread->state = APTO;
  deactivateRunningThread();

  // get the next thread with the scheduler
  TCB_t* nextThread;
  nextThread = getNextThread();
  if(nextThread == NULL) _RAISE_NEXT_THREAD_ERROR();
  

  DEBUG_PRINT("yielding thread %d (tid) \n", runningThread->tid);
  
  runThread(runningThread, nextThread);
  return SUCCESS;
}




int cjoin(int tid)
{
  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  // check whether target thread exists
  TCB_t* toBeWaited = exists(tid);
  if(toBeWaited == NULL) _RAISE_DOESNT_EXIST_ERROR(tid);

  List* waitingList = getWaiting();
  // is there any other thread waiting for the target thread?
  if(waitingList_isBeingWaited(waitingList, tid) != NULL) {
    STDERR_INFO("Thread %d is being waited by another thread \n", tid);
    return ERROR;
  }
  // if not, adds the thread to a waiting list
  WaitingInfo* waitingInfo = (WaitingInfo*) calloc(1, sizeof(WaitingInfo));
  if(waitingInfo == NULL) return ERROR; // calloc error

  TCB_t* runningThread = getRunningThread();
  if(runningThread == NULL) _RAISE_RUNNING_THREAD_ERROR();

  waitingInfo->waiting = runningThread->tid; // who is waiting
  waitingInfo->beingWaited = toBeWaited->tid; // target thread

  lockRunningThread();

  list_push_back(waitingList, &waitingInfo);


  // get the next thread with the scheduler
  TCB_t* nextThread = getNextThread();
  if(nextThread == NULL) _RAISE_NEXT_THREAD_ERROR();

  

  DEBUG_PRINT("Thread TID: %d \t waiting for thread TID: %d \n",
                waitingInfo->waiting, 
                waitingInfo->beingWaited);

  runThread(runningThread, nextThread);
  return SUCCESS;
}




int csem_init(csem_t *sem, int count)
{
  if(count <= 0) return ERROR;

  if(sem == NULL) {
    sem = (csem_t*) calloc(1, sizeof(csem_t));
    if(sem == NULL) return ERROR;
  }

  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  sem->count = count;

  sem->threads = (List*) calloc(1, sizeof(List));
  if(sem->threads == NULL) return ERROR;

  if(list_new(sem->threads, sizeof(TCB_t*), NULL)) //to-do: check if need a free function bind
    return SUCCESS;
  else
    return ERROR;
}




int cwait(csem_t *sem)
{
  if(sem == NULL) return ERROR;

  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  // check whether there is a free resource to be
  // used by the running thread
  if(sem->count > 0){
    // if so, we take the resource and give it to the running thread
    sem->count--;
  }
  else {
    // if not, we lock the current thread and put it
    // into sem's queue
    TCB_t* runningThread = getRunningThread();
    if(runningThread == NULL) _RAISE_RUNNING_THREAD_ERROR();

    lockThreadForMutex(sem, runningThread);

    TCB_t* nextThread = getNextThread();
    if(nextThread == NULL) _RAISE_NEXT_THREAD_ERROR();

    runThread(runningThread, nextThread);
    return SUCCESS;
  }

  return SUCCESS;
}




int csignal(csem_t *sem)
{
  if(sem == NULL) return ERROR;
  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  sem->count++;
  unlockMutexThread(sem);

  return SUCCESS;
}




int cidentify(char* name, int size)
{
  if(_mainTCB_created == false) 
    if(_createMainTCB() == ERROR) return ERROR;

  if(size < sizeof(devs)) return ERROR;
  else {
    strncpy(name, devs, sizeof(devs));
    return SUCCESS;
  }
}