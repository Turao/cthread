#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>

#include "utils.h"
#include "scheduler.h"
#include "list.h"

#define SUCCESS 0
#define ERROR -1

int32_t _GLOBAL_TID = 1;

Apts* _apts = NULL;

bool _apts_initialized = false;
#define _INIT_APTS() {_initActive(); _apts_initialized = true;}

List _locked;
bool _locked_initialized = false;
#define _INIT_LOCKED() {list_new(&_locked, sizeof(TCB_t*), NULL); _locked_initialized = true;}

List _waiting;
bool _waiting_initialized = false;
#define _INIT_WAITING() {list_new(&_waiting, sizeof(WaitingInfo*), NULL); _waiting_initialized = true;}

TCB_t* _running;




int32_t _initActive()
{
  _apts = (Apts*) calloc(1, sizeof(Apts));
  if(_apts == NULL) return ERROR;

  if(list_new(&(_apts->p0), sizeof(TCB_t*), NULL) &&
     list_new(&(_apts->p1), sizeof(TCB_t*), NULL) &&
     list_new(&(_apts->p2), sizeof(TCB_t*), NULL) &&
     list_new(&(_apts->p3), sizeof(TCB_t*), NULL))
    return SUCCESS;
  else
    return ERROR;
}




void runThread(TCB_t* current_thread, TCB_t* next_thread)
{
  assert(next_thread != NULL);

  setRunningThread(next_thread);
  if(current_thread != NULL) {
    swapcontext(&current_thread->context, &next_thread->context);
  }
  else {
    setcontext(&next_thread->context);
  }
}


// get next thread
// using: FIFO with priorities
TCB_t* getNextThread()
{
  if(!_apts_initialized) _INIT_APTS();
  TCB_t* next_thread = NULL;
  
  if(!list_empty(&_apts->p0))
    next_thread = *(TCB_t**) list_pop_front(&_apts->p0);
  else
    if(!list_empty(&_apts->p1))
      next_thread = *(TCB_t**) list_pop_front(&_apts->p1);
    else
      if(!list_empty(&_apts->p2))
        next_thread = *(TCB_t**) list_pop_front(&_apts->p2);
      else
        if(!list_empty(&_apts->p3))
          next_thread = *(TCB_t**) list_pop_front(&_apts->p3);

  if(next_thread == NULL) {
    // there are no threads to run
    // probably an error made by the scheduler
    STDERR_INFO("No thread to run \n");
    return NULL;
  }
  else return next_thread;
}




void setRunningThread(TCB_t* thread)
{
  _running = thread;
  thread->state = EXECUCAO;
}




// this function is called whenever a thread function ends
int32_t terminateThread()
{
  if(!_waiting_initialized) _INIT_WAITING();

  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();

  // main thread finished, we must free the memory blocks calloc'd
  if(_running->tid == 0) {_cleanup(); return SUCCESS;}

  DEBUG_PRINT("\n\t- Thread %3i is finished. -\n", runningThread->tid);
  // checa se a thread estava sendo esperada por outra
  // se estiver, libera a thread na fila de bloqueados
  WaitingInfo* waitingInfo = waitingList_isBeingWaited(&_waiting, _running->tid);
  if(waitingInfo != NULL) unlockThread(waitingInfo->waiting);
  
  // thread cleanup
  free(_running->context.uc_stack.ss_sp); // frees stack pointer
  free(_running); // frees thread pointer
  _running = NULL;
  
  TCB_t* nextThread = getNextThread();
  if(nextThread != NULL) runThread(NULL, nextThread);
  return SUCCESS;
}




void enqueueApts(TCB_t* thread)
{
  if(!_apts_initialized) _INIT_APTS();
  _enqueue(_apts, thread);
}




// enqueue based on creation credits
void _enqueue(Apts* queue, TCB_t* thread)
{
  switch(thread->prio)
  {
    case 0:
      list_push_back(&queue->p0, &thread);  
      break;

    case 1:
      list_push_back(&queue->p1, &thread);
      break;

    case 2:
      list_push_back(&queue->p2, &thread);
      break;

    case 3:
      list_push_back(&queue->p3, &thread);
      break;

    default:
      STDERR_INFO("Couldn't enqueue thread. Invalid priority. \n");
      break;
  }
}




int32_t updatePriorityQueue(TCB_t* thread)
{
  if(!_apts_initialized) _INIT_APTS();
  if(!_locked_initialized) _INIT_LOCKED();
  if(!_waiting_initialized) _INIT_WAITING();

  // seeks for the given thread within the apts lists
  // takes it out and enqueues it again into the correct list
  list_remove(&_apts->p0, _compare_tid, &thread->tid);
  list_remove(&_apts->p1, _compare_tid, &thread->tid);
  list_remove(&_apts->p2, _compare_tid, &thread->tid);
  list_remove(&_apts->p3, _compare_tid, &thread->tid);

  _enqueue(_apts, thread);

  return SUCCESS;
}



int32_t get_new_TID()
{
  int32_t tid = _GLOBAL_TID;
  _GLOBAL_TID++;
  return tid;
}




TCB_t* getRunningThread()
{
  return _running;
}


// print thread attributes
bool print_thread(void* thread)
{
  assert(thread != NULL);

  TCB_t* _thread = *(TCB_t**) thread;
  printf("\n");
  printf(" * TID: %d \n", _thread->tid);
  printf(" * State: %d \n", _thread->state);
  printf(" * Priority: %d \n", _thread->prio);
  printf("\n");
  return false;
}




// enqueue in the correspondent active group queue
int32_t deactivateRunningThread()
{
  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();

  enqueueApts(_running);

  _running = NULL; // safety measure

  return SUCCESS;
}





// insert it in the locked threads list
int32_t lockRunningThread()
{
  if(!_locked_initialized) _INIT_LOCKED();

  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();

  _running->state = BLOQUEADO;

  list_push_back(&_locked, &_running);
  return SUCCESS;
}




// takes the thread tid from the locked threads list
// and enqueues it in the correspondent apts group queue
int32_t unlockThread(int32_t tid)
{
  if(!_locked_initialized) _INIT_LOCKED();

  TCB_t* thread = *(TCB_t**) list_find(&_locked, _compare_tid, &tid);
  if(thread == NULL) _RAISE_DOESNT_EXIST_ERROR(tid);

  list_remove(&_locked, _compare_tid, &tid);

  _running->state = APTO;
  enqueueApts(thread);

  return SUCCESS;
}




// insert thread in mutex locked threads list
int32_t lockThreadForMutex(csem_t *sem, TCB_t* thread)
{
  if(sem == NULL) return ERROR;

  thread->state = BLOQUEADO;

  list_push_back(sem->threads, &thread);

  return SUCCESS;
}




// takes the first thread from the mutex locked threads list
int32_t unlockMutexThread(csem_t *sem)
{
  if(!_locked_initialized) _INIT_LOCKED();

  if(sem == NULL) return ERROR;

  TCB_t* toBeUnlocked = NULL;
  if(!list_empty(sem->threads)) {
    toBeUnlocked = *(TCB_t**) list_pop_front(sem->threads);
  }

  if(toBeUnlocked != NULL)
  {
    toBeUnlocked->state = APTO;
    enqueueApts(toBeUnlocked);
  }
  return SUCCESS;
}



TCB_t* exists(int32_t tid)
{
  if(!_apts_initialized) _INIT_APTS();
  if(!_locked_initialized) _INIT_LOCKED();
  if(!_waiting_initialized) _INIT_WAITING();

  TCB_t** result = NULL;

  // check if thread is running
  if(_running->tid == tid) return _running;


  // check if thread is in the active apts group
  // highest priority queue
  result = (TCB_t**) list_find(&_apts->p0, _compare_tid, &tid);
  if(result != NULL) return *result;  

  result = (TCB_t**) list_find(&_apts->p1, _compare_tid, &tid);
  if(result != NULL) return *result;

  result = (TCB_t**) list_find(&_apts->p2, _compare_tid, &tid);
  if(result != NULL) return *result;

  // lowest priority queue
  result = (TCB_t**) list_find(&_apts->p3, _compare_tid, &tid);
  if(result != NULL) return *result;


  // check if thread is in the locked threads list
  result = (TCB_t**) list_find(&_locked, _compare_tid, &tid);
  if(result != NULL) return *result;

  return NULL;
}




bool _compare_tid(void* thread, void* tid)
{
  TCB_t* _thread = *(TCB_t**) thread;
  if(_thread == NULL) return false;

  int32_t _tid = *(int32_t*) tid;
  return _thread->tid == _tid ? true : false;
}




Apts* getActive()
{
  return _apts;
}




List* getLocked()
{
  return &_locked;
}




List* getWaiting()
{
  return &_waiting;
}




void _cleanup()
{
  free(_apts);
  _apts = NULL;

  free(_running);
  _running = NULL;
}