#
# Makefile ESQUELETO
#
# OBRIGATÓRIO ter uma regra "all" para geração da biblioteca e de uma
# regra "clean" para remover todos os objetos gerados.
#
# NECESSARIO adqueuear este esqueleto de makefile para suas necessidades.
#
# OBSERVAR que as variáveis de ambiente consideram que o Makefile está no diretótio "cthread"
# 

CC=gcc -m32
LIB_DIR=./lib/
INC_DIR=./include/
BIN_DIR=./bin/
SRC_DIR=./src/

all: mkbin list waiting cthread scheduler lib_cthread clean_obj

lib_cthread:
	rm -rf $(LIB_DIR)
	mkdir $(LIB_DIR)
	ar crs $(LIB_DIR)libcthread.a $(BIN_DIR)cthread.o $(BIN_DIR)list.o $(BIN_DIR)waiting.o $(BIN_DIR)scheduler.o

scheduler:
	$(CC) -c $(SRC_DIR)scheduler.c -I$(INC_DIR) -Wall
	mv scheduler.o $(BIN_DIR)
	
cthread:
	$(CC) -c $(SRC_DIR)cthread.c -I$(INC_DIR) -Wall
	mv cthread.o $(BIN_DIR)

waiting:
	$(CC) -c $(SRC_DIR)waiting.c -I$(INC_DIR) -Wall
	mv waiting.o $(BIN_DIR)

list:
	$(CC) -c $(SRC_DIR)list.c -I$(INC_DIR) -Wall
	mv list.o $(BIN_DIR)

mkbin:
	mkdir -p $(BIN_DIR)

clean_obj:
	rm -rf $(BIN_DIR)*.o

clean:
	rm -rf $(LIB_DIR)*.a $(BIN_DIR)*.o $(SRC_DIR)*~ $(INC_DIR)*~ *~